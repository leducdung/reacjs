
import { useState, useEffect } from "react";
import axios from "axios";
//state vs props ??
//react-hook = state management
function Tabs(props) {
  const {tokens , pickType} = props
  const [tabs, setTabs] = useState([]);
  const [currentTabIndex, setCurrentTabIndex] = useState(0);
  const [typeValue, setTypeValue] = useState("cat");

  useEffect(async () => {
    const token =tokens

    const result = await axios.get(`https://api.petfinder.com/v2/types`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    console.log(result);
    setTabs(result?.data?.types);
  }, []);
  if (!tabs || tabs.length === 0) {
    return <div> There is no data yet. please wait</div>;
  }

  return (
    <div key='1'>
      <select key='122' onChange={()=> {pickType(event.target.value) }}>
        {tabs.map((typeName , index) => {
          return (
            <>
              <option  value={typeName.name} key={index}>{typeName.name} </option>

            </>
          );
        })}

      </select>
    </div>

  );
}
export default Tabs;

