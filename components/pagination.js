
import CombonentPageNumber from "../components/NumberPage";
export default function CombonentPagination(props) {
  const { pagination, goPaination ,setpage ,page , color} = props;

  const eventPreviousAndNext =(index)=>{
    let newPage

    if(page==1){
      newPage = page
    }

    if(page == pagination.total_pages){
      newPage = pagination.total_pages
    }

    if (page!==1 && page!== pagination.total_pages) {
     newPage = page+ index
    }

    setpage(newPage)
  }

  return (
    <div>
      <div className="pagination">
        <button href="#" onClick={() => goPaination(1)}>
          «
        </button>
        <button onClick={()=>{eventPreviousAndNext(-1)}}>previous</button>

          <CombonentPageNumber
          page= {page}
          goPaination={goPaination}
          pagination={pagination}
          color={color}
          ></CombonentPageNumber>

        <button onClick={()=>{eventPreviousAndNext(+1)}}>next</button>
        <button href="#" onClick={() => goPaination(pagination.total_pages)}>
          »
        </button>
      </div>
    </div>
  );
}
