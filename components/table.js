import CombonentName from "../components/name";
import { useState, useEffect } from "react";
import CombonentAge from "../components/age";
import CombonentType from "../components/type";
import CombonentAction from "../components/action";
export default function CombonentTable(props) {
  const { animals, deleteAnimal,addAnimail } = props;
  const [valueNameAnimail, setValueNameAnimail] = useState('valueName');
  const [valueAgeAnimail, setValueAgeAnimail] = useState('valueAge');
  const [valueTypeAnimail, setValueTypeAnimail] = useState('valueType');
  return (
    <div
      style={{
        display: "table",
        padding: "2px",
        borderRadius: "5px",
        border: "1px solid #999",
      }}
    >
      <table style={{ borderCollapse: "collapse" }}>
        <thead>
          <tr style={{ backgroundColor: "red" }}>
            <th>name</th>
            <th>age</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <>
          <tr>
              <td><input id="valueName" value={valueNameAnimail} onChange={(event)=> setValueNameAnimail(event.target.value)}></input></td>
              <td><input id="valueAge" value={valueAgeAnimail} onChange={(event)=> setValueAgeAnimail(event.target.value)}></input></td>
              <td><input id="valueType" value={valueTypeAnimail} onChange={(event)=> setValueTypeAnimail(event.target.value)} ></input></td>
              <td><button onClick={() => addAnimail(valueNameAnimail,valueAgeAnimail,valueTypeAnimail)}  >ADD</button></td>

            </tr>
            {animals.map((animalItem, index) => {
              return (
                <tr key={index}>
                  <CombonentName name={animalItem.name}></CombonentName>
                  <CombonentAge age={animalItem.age}></CombonentAge>
                  <CombonentType type={animalItem.type}></CombonentType>
                  <CombonentAction action={animalItem.action} index={index} deleteAnimal={deleteAnimal}></CombonentAction>
                </tr>
              );
            })}
          </>
        </tbody>
      </table>
    </div>
  );
}
