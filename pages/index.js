
import Tabs from "../components/tabs";
import CombonentPagination from "../components/pagination";
import CombonentTable from "../components/table";
import { useState, useEffect } from "react";
import axios from "axios";
//state vs props ??
//react-hook = state management
function HomePage() {
  const [types, setTypes] = useState("Dog");
  const [animals, setanimals] = useState([]);
  const [page, setpage] = useState(2);
  const [tokens, setTokens] = useState("");
  const [pagination, setPagination] = useState([])
  const [color, setColor] = useState("white");
  useEffect(async () => {
    const CATEGORY = "animals";
    const ACTION = "";
    const token =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJFTDJZMXZQWkdkOEJvTzRQOHpxZU51RkIwRk1kWWZJQW5WRjRoT1NLYU05ZDRVYzJ6ciIsImp0aSI6IjZhZmY4NzkzMTJhMjU3ZDlkMzA1NzZjNTAwYzA1MzBmOTFjNDllYjVmYWZjMmZmN2RhYTM1ODNkYTg3MmJjMzU1YmRkODIxMjA0Yzc0OTMyIiwiaWF0IjoxNjE1NDc2MzEyLCJuYmYiOjE2MTU0NzYzMTIsImV4cCI6MTYxNTQ3OTkxMiwic3ViIjoiIiwic2NvcGVzIjpbXX0.gKDuO-GFS92m14lLRcqGE3jk6NwGAJIqVjFVLWChLxj_Oq52Oe_WrdXJiFOZCPjINwxglaQIATQmJIUfG0RkbnFv34Y2IWtgf0ElarhMq7T6-OTONCfLYO6Nlm3B0dXUzoddWndRvelyo2tac30r88IYqlESFAblYD58BROBscqPsQvAsSnaMbtgVOC1n3cnI3ApQ4t9oBSXW69zR3LoK2jd3lc_XwFgOdDLCzIyZWNV5-QX04ng4D5BZl_ojSBQoudi85TOgS-XKV6ICqqapLrMUAPJGdZG7GLGXM8adwla_SLrIQ_DtNfdIDWPl3XrZV238hHrQHYzEPvnLEprEQ";
    setTokens(token)
    const result = await axios.get(
      `https://api.petfinder.com/v2/${CATEGORY}/${ACTION}`,
      {
        params: {
          type: types,
          page: String(page) ,
        },
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    console.log(result);
    setanimals(result?.data?.animals);
    setPagination(result?.data?.pagination)

  }, [types,page ,color]);
   if (!animals || animals.length === 0) {
    return <div> There is no data yet. please wait</div>;
  }
  const deleteAnimal = (index) => {
    const newAnimals = [];
    for (let i = 0; i < animals.length; i++) {
      if (i !== index) {
        newAnimals.push(animals[i]);
      }
    }
    setanimals(newAnimals);
  };
  const pickType = (type) => {
    setTypes(type)
  }
  const goPaination = (a,b) =>{
    if (a ===b) {
      setColor("red")
    } else {
      setColor("blue")
    }
    setpage(a)

  }


  const addAnimail = (name , age, type) =>{
    const NewAddAnimal = [{name:name, age:age,type:type},...animals]
    setanimals(NewAddAnimal)
  }
  return (
    <div>
       <Tabs tokens={tokens} pickType ={pickType} > </Tabs>
      <CombonentTable animals={animals}
      deleteAnimal ={deleteAnimal}
      addAnimail={addAnimail}
      ></CombonentTable>
      <CombonentPagination
       pagination={pagination}
       goPaination={goPaination}
       setpage ={setpage}
       page= {page}
       color= {color}
       ></CombonentPagination>

    </div>

  );
}
export default HomePage;
