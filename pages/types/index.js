import { useState, useEffect } from "react";
import axios from "axios";
//state vs props ??
//react-hook = state management
function Tabs(props) {
  const [tabs, setTabs] = useState([]);
  const [currentTabIndex, setCurrentTabIndex] = useState(0);
  const token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJFTDJZMXZQWkdkOEJvTzRQOHpxZU51RkIwRk1kWWZJQW5WRjRoT1NLYU05ZDRVYzJ6ciIsImp0aSI6IjMyOTNkN2E0YTE0NWUyMTFkOWMyM2FhNGQ5YmYyZjYzNjJkZTU5ZDVlMGEzMjkxNDAwYTZiYTUxOGY2YzAwZmMwZDgwYThhMjRhZmIyMDgwIiwiaWF0IjoxNjE1MTM3NjIxLCJuYmYiOjE2MTUxMzc2MjEsImV4cCI6MTYxNTE0MTIyMSwic3ViIjoiIiwic2NvcGVzIjpbXX0.GAWnkpsPJdNcom96rw3WJdAuij_z8g9LdOKaeG5EQFhElyJgoDER_Vsw2mREaviQjVytElQKTUtpfN3_zoszYWNHeYVqD7YJ3v4PquM4XZC28Gih7BGSPAXbI-bvHpoq7GBnC3k6AxdqoscqUipxytPma06itilqGsukBv0ht2shOZwtGSUIvy66yL-d7tuZXhqn3VNfmpzVlN3aDzrW3zM3kcQQJqFtNj0yeoec3PvZtG5psP9z38_mFHkjTrlC0bCk2zkY-bWtoHqzZWbPPVV3o4_8MeNJfZ50GQfDcfAwPOSTicHkfPRLx3TsXcPQPD7U4LCbGlujTdWDzjYkCA";

  useEffect(async () => {

    const result = await axios.get(`https://api.petfinder.com/v2/types`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    console.log(result);
    setTabs(result?.data?.types);
  }, []);
  if (!tabs || tabs.length === 0) {
    return <div> There is no data yet. please wait</div>;
  }

  return (
    <div>
      <select>
        {tabs.map((typeName , index) => {
          return (
            <>
              <option key={index}>{typeName.name}</option>
            </>
          );
        })}
      </select>
    </div>
  );
}
export default Tabs;
